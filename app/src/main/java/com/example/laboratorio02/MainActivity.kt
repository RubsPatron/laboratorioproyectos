package com.example.laboratorio02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //1.- Evento onclick del botón
        btnVerificar.setOnClickListener {

            //2.-Recuperar el input del ano de nacimiento

            if (
                !edtNacimiento.text.toString().isNullOrEmpty()
            ) {

                val anioNacimiento = edtNacimiento.text.toString().toInt()

                //IF-ELSE

                if (anioNacimiento >= 1994 && anioNacimiento <= 2010) {
                    tvResultadoGeneracion.text = "Estas dentro de la GENERACION Z"
                } else if (anioNacimiento >= 1981 && anioNacimiento <= 1993) {
                    tvResultadoGeneracion.text = "Estas dentro de la GENERACION Y"
                } else if (anioNacimiento >= 1969 && anioNacimiento <= 1980) {
                    tvResultadoGeneracion.text = "Estas dentro de la GENERACION X"
                } else if (anioNacimiento >= 1949 && anioNacimiento <= 1968) {
                    tvResultadoGeneracion.text = "Estas dentro de la GENERACION BABY BOOM"
                } else if (anioNacimiento >= 1930 && anioNacimiento <= 1948) {
                    tvResultadoGeneracion.text = "Estas dentro de la GENERACION SILENT"
                } else {
                    tvResultadoGeneracion.text = "No perteneces a ningun GENERACIÓN"
                }
            } else {
                Toast.makeText(this, "El año no puede estar vacio", Toast.LENGTH_SHORT).show()
            }
        }
    }
}